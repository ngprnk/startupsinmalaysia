class AddDetailsToAddstartup < ActiveRecord::Migration
  def change
    add_column :addstartups, :title, :string
    add_column :addstartups, :founders, :string
    add_column :addstartups, :description, :string
  end
end
