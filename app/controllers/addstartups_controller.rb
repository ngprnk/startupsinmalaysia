class AddstartupsController < ApplicationController

	def new
		@startup=Addstartup.new
	end

	def create
		@startup=Addstartup.create(startup_params)

	end


	#private params setup for create controller.
	private
	def startup_params
		params.require(:addstartup).permit(:title,:founders,:description)
	end


end
