class Info < ActiveRecord::Base

	has_attached_file :image, styles: {large: "600*600>", medium: "300*200>",thumb: "220*140%"}
	validates_attachment_content_type :image , content_type: /\Aimage\/.*\Z/
	validates :image, presence: true
	validates :title, presence: true
	validates :description , presence: true


end
